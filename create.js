const fs = require('fs');

const createPerson = function (person) {
    fs.writeFileSync('./person.json', JSON.stringify(person));
    return person;
}

const nilam = createPerson( [{
    name: 'Nilam',
    age: 20,
    address: 'Lampung'
}] )

module.exports = createPerson;
