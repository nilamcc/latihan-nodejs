//import module
const fs = require('fs');
const express = require('express');

//penggunaan fs module untuk membaca isi file
const read = fs.readFileSync('text.txt', 'utf-8');
console.log(read);

//penggunaan fs module untuk membuat file dan isinya
fs.writeFileSync('test.txt', "I love Binar");
const readFile = fs.readFileSync('test.txt', 'utf-8');
console.log(readFile);

//import file yang berisi data dalam bentuk json
const person = require("./person.json");
console.log(person);

//5. render halaman localhost:3000/data yg berisi list item dari person.json(dari fs module yg di reading material)
const app = express();

app.get('/', (req, res) => {
    res.render('index.ejs', {
        data: person
    })
})

app.listen(3000);
